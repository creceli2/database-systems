"""
Name: Will Crecelius
Time To Completion: 20 hours
Comments:
This SQL module can incorporate properly structured 
Create, Insert, and Select statements.
CSE 480 Database Systems Project 2


Sources:
     wiki.python Sorting mini-HOW TO https://wiki.python.org/moin/HowTo/Sorting
"""
import string
from operator import itemgetter, attrgetter, methodcaller

_ALL_DATABASES = {}


class Connection(object):
    def __init__(self, filename):
        """
        Takes a filename, but doesn't do anything with it.
        (The filename will be used in a future project).
        """
        self.name = ''
        self.cols = []
        pass

    def execute(self, statement):
        """
        Takes a SQL statement.
        Returns a list of tuples (empty unless select statement
        with rows to return).
        """
        tokens = self.tokenize(statement)
        if(tokens[0] == 'CREATE'):
            for i in range(0, len(tokens)):
                if(tokens[i] == '('):
                    start_value_paren = i;
                if(tokens[i] == ')'):
                    end_value_paren = i;
                    
            cols = tokens[start_value_paren+1:end_value_paren]
            cols = [x for x in cols if x != ',']
                    
            table = Table(tokens[2], [], cols)
            _ALL_DATABASES[tokens[2]] = table
            return []
            
        if(tokens[0] == 'INSERT'):
            name = tokens[2]
            start_value_paren = 0
            end_value_paren = 0
            for i in range(0, len(tokens)):
                if(tokens[i] == '('):
                    start_value_paren = i;
                if(tokens[i] == ')'):
                    end_value_paren = i;
                    
            vals = tokens[start_value_paren+1:end_value_paren]
            vals = [x for x in vals if x != ',']
            
            _ALL_DATABASES[name].add_row(vals)
            return []
            
        if(tokens[0] == 'SELECT'):
            return_array = []
            #Get the column(s) to return
            #Get the index(es) of the column(s) in the column list
            #Get the rows
            #Get the values from the rows at those indecies
            name_index = tokens.index('FROM')+1
            tName = tokens[name_index]
            table = _ALL_DATABASES[tName]
            
            #Get the column names we want
            start_value_paren = 0
            end_value_paren = 0
            for i in range(0, len(tokens)):
                if(tokens[i] == 'SELECT'):
                    start_value_paren = i;
                if(tokens[i] == 'FROM'):
                    end_value_paren = i;
                    
            wanted = tokens[start_value_paren+1:end_value_paren]
            wanted = [x for x in wanted if x != ',']
            wanted_indexes = []
            
            for wanted_col in wanted:
                if(wanted_col in _ALL_DATABASES[tName].getCols()):
                    wanted_indexes.append( _ALL_DATABASES[tName].getCols().index(wanted_col))
                    
            for i in range(0, len(wanted_indexes)):
                wanted_indexes[i] = int(wanted_indexes[i]/2)
            
            rows = _ALL_DATABASES[tName].getRows()
            
            for row in rows:
                tack = []
                for index in wanted_indexes:
                    tack.append(row[index])
                return_array.append(tuple(tack))
                
            
            if '*' in tokens:
                return_array = table.getRows()
                wanted = table.getCols()
                
            if(tokens.index('ORDER')):
                start_value_paren = 0
                end_value_paren = 0
                for i in range(0, len(tokens)):
                    if(tokens[i] == 'BY'):
                        start_value_paren = i;
                    if(tokens[i] == ';'):
                        end_value_paren = i;
                specs = tokens[start_value_paren+1:end_value_paren]
                specs = [x for x in specs if x != ',']
                
                specs_ndx = []
                
                for i in range(0, len(specs_ndx)):
                    specs_ndx[i] = int(specs_ndx[i]/2)
                
                for i in specs:
                    specs_ndx.append(wanted.index(i))
                    
                for i in range(0, len(specs_ndx)):
                    specs_ndx[i] = int(specs_ndx[i]/2)
                
                ordered = table.order(return_array, specs_ndx)
                
                return ordered
                
            return return_array
            
        
        
        
        return []

    def close(self):
        """
        Empty method that will be used in future projects
        """
        pass
    
    
    #cite: Dr. Nahums Week 3 Tokenize Video
    '''This set of functions turns a string of SQL command text 
    	into a string list of workable tokens'''
    	
    '''Tokenize functions'''
    def collect_chars(self, query, allowed):
        letters = []
        for letter in query:
            if letter not in allowed:
                break
            letters.append(letter)
        return "".join(letters)
    
    def remove_white(self, query, tokens):
        whitespace = self.collect_chars(query, string.whitespace)
        return query[len(whitespace):]
        
    def remove_word(self, query, tokens):
        word = self.collect_chars(query, string.ascii_letters + "_" + string.digits)
        if(word == 'NULL'):
            tokens.append(None)
            return query[len(word):]
        tokens.append(word)
        return query[len(word):]
        
    def remove_text(self, query, tokens):
        assert query[0] == "'"
        query = query[1:]
        end_quote_index = query.find("'")
        text = query[:end_quote_index]
        tokens.append(text)
        query = query[end_quote_index+1:]
        return query
    
    
    def remove_integer(self, query, tokens):
        int_str = self.collect_chars(query, string.digits)
        tokens.append(int(int_str))
        return query[len(int_str):]
        
    def remove_number(self, query, tokens):
        query = self.remove_integer(query, tokens)
        if query[0] == ".":
            whole_str = tokens.pop()
            whole_str = (str(whole_str))
            query = query[1:]
            query = self.remove_integer(query, tokens)
            frac_str = tokens.pop()
            frac_str = (str(frac_str)) 
            float_str = whole_str + "." + frac_str
            tokens.append(float(float_str))
            
        int_str = tokens.pop()
        tokens.append(int_str)
        return query
    
    def tokenize(self, query):
        tokens = []
        while query:
            
            old_query = query
            
            if query[0] in string.whitespace:
                query = self.remove_white(query, tokens)
                continue
            
            if query[0] in (string.ascii_letters + "_"):
                query = self.remove_word(query, tokens)
                continue
            
            if query[0] in "(),;*":
                tokens.append(query[0])
                query = query[1:]
                continue
                
            if query[0] == "'":
                query = self.remove_text(query, tokens)
                continue
            
            if query[0] in string.digits:
                query = self.remove_number(query, tokens)
                
                
            if len(query) == len(old_query):
                raise AssertionError("Query didnt get shorter")
        
        return tokens
        '''END OF TOKENIZER'''
        


def connect(filename):
    """
    Creates a Connection object with the given filename
    """
    return Connection(filename)

class Database:
    pass

'''SQL Table class'''
class Table:
    def __init__(self, name, values, cols):
        self.name = name
        self.cols = cols
        self.rows = []
        if self.name not in _ALL_DATABASES:
            self.create_table(name)
        
        pass
    
    def getRows(self):
        return self.rows
    
    def add_row(self, row):
        self.rows.append(tuple(row))
        return
    
    def create_table(self, name):
        _ALL_DATABASES[self.name] = []
        return
        
    def getCols(self):
        return self.cols
        
    
    def order(self, array, specs):
        ret = []
        if(len(specs) == 1):
            ret = sorted(array, key=itemgetter(specs[0]))
        if(len(specs) == 2):
            ret = sorted(array, key=itemgetter(specs[0], specs[1]))
        if(len(specs) == 3):
            ret = sorted(array, key=itemgetter(specs[0], specs[1], specs[2]))
        if(len(specs) == 4):
            ret = sorted(array, key=itemgetter(specs[0], specs[1], specs[2], specs[3]))
        
        return ret
            
    
    
    pass

'''Below are a list of functions and tests I used during development.'''
def main():
	
    conn = connect("test.db")
    conn.execute("CREATE TABLE students (col_1 INTEGER, _col2 TEXT, col_3_ REAL);")
    conn.execute("INSERT INTO students VALUES (33, 'hi', 4.5);")
    conn.execute("INSERT INTO students VALUES (3, 'hweri', 4.5);")
    conn.execute("INSERT INTO students VALUES (75842, 'string with spaces', 3.0);")
    conn.execute("INSERT INTO students VALUES (623, 'string with spaces', 3.0);")
    result = conn.execute("SELECT * FROM students ORDER BY col_3_, col_1;")
    result_list = list(result)
    
    expected = [(623, 'string with spaces', 3.0), (75842, 'string with spaces', 3.0), (3, 'hweri', 4.5), (33, 'hi', 4.5)]
    
    print("expected:",  expected)
    print("student: ",  result_list)
    assert expected == result_list
    # conn = connect("test.db")
    # conn.execute("CREATE TABLE students (col_1 INTEGER, _col2 TEXT, col_3_ REAL);")
    # conn.execute("INSERT INTO students VALUES (33, 'hi', 4.5);")
    # conn.execute("INSERT INTO students VALUES (7842, 'string with spaces', 3.0);")
    # conn.execute("INSERT INTO students VALUES (7, 'look a null', NULL);")
    # result = conn.execute("SELECT * FROM students ORDER BY col_1;")
    # result_list = list(result)
    
    # expected = [(7, 'look a null', None), (33, 'hi', 4.5), (7842, 'string with spaces', 3.0)]
    
    # print("expected:",  expected)
    # print("student: ",  result_list)
    # conn = connect("test.db")
    # conn.execute("CREATE TABLE students (num INTEGER, num2 INTEGER);")
    # conn.execute("INSERT INTO students VALUES (3, 5);")
    # conn.execute("INSERT INTO students VALUES (4, 1);")
    # conn.execute("INSERT INTO students VALUES (7, 133);")
    # conn.execute("INSERT INTO students VALUES (10, 78);")
    # result = conn.execute("SELECT num, num2 FROM students ORDER BY num2;")
    # result_list1 = list(result)
    
    # expected1 = [(4, 1), (3, 5), (10, 78), (7, 133)]
    # print("expected1:",  expected1)
    # print("student1:",  result_list1)
    # print()
    # print("SECOND RUN!!!!!!!!")
    
    # conn = connect("test.db")
    # conn.execute("CREATE TABLE students (num INTEGER, num2 INTEGER);")
    # conn.execute("INSERT INTO students VALUES (3, 5);")
    # conn.execute("INSERT INTO students VALUES (4, 1);")
    # conn.execute("INSERT INTO students VALUES (7, 133);")
    # conn.execute("INSERT INTO students VALUES (48, 133);")
    # conn.execute("INSERT INTO students VALUES (10, 78);")
    # result = conn.execute("SELECT num2, num FROM students ORDER BY num2;")
    # result_list2 = list(result)
    
    # expected2 = [(1, 4), (5, 3), (78, 10), (133, 7), (133, 48)]
    
    # print("expected2:",  expected2)
    # print("student2:",  result_list2)
    #assert expected == result_list
    
    # print("expected:",  expected)
    # print("student:",  result_list)
    # assert expected == result_list
    # conn = connect("test.db")
    # conn.execute("CREATE TABLE students (num INTEGER, name TEXT);")
    # conn.execute("INSERT INTO students VALUES (4, 'four');")
    # conn.execute("INSERT INTO students VALUES (1, 'one');")
    # conn.execute("INSERT INTO students VALUES (5, 'five');")
    # conn.execute("INSERT INTO students VALUES (3, 'three');")
    # conn.execute("INSERT INTO students VALUES (2, 'two');")
    # result = conn.execute("SELECT num, name FROM students ORDER BY num;")
    
    # #print(_ALL_DATABASES['students'].getCols())
    # print(_ALL_DATABASES['students'].getRows())
    
    
    # print("expected:",  expected)
    # print("student:",  result_list)
    # assert expected == result_list
        
main()